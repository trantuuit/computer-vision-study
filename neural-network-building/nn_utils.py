import numpy as np
import matplotlib.pyplot as plt
import imageio

np.random.seed(1)

class NNUtils():

    @staticmethod
    def initialize_parameters_deep(layer_dim):
        """
        Argument:
        layer_dim -- python array containing the dimension of each layer in our network
        
        Return:
        parameters -- python dictionary containing your parameters 'W1, b1, W2, b2, ..., Wl, bl':
            Wl -- weight matrix of shape (layer_dim[l], layer_dim[l - 1])
            bl -- bias vector of shape (layer_dim[l], 1)
        """
        parameters = {}
        np.random.seed(1)
        
        for l in range(1, len(layer_dim)):
            parameters['W' + str(l)] = np.random.randn(
                layer_dim[l], 
                layer_dim[l - 1])/ np.sqrt(layer_dim[l-1]) #* 0.01
            parameters['b' + str(l)] = np.zeros((layer_dim[l], 1))
        return parameters

    @staticmethod
    def sigmoid(Z):
        """
        Compute the sigmoid of Z
        Argument:
        Z -- A scalar or numpy array of any size
        
        Return:
        A -- post-activation parameter, of the same shape as Z
        Z -- pre-activation parameter
        """
        A = 1/(1 + np.exp(-Z))
        return A, Z

    @staticmethod
    def sigmoid_backward(dA, activation_cache):
        """
        Implement the backward propagation for a single sigmoid
        
        Arguments:
        dA -- post-activation gradient, of any shape
        activation_cache -- 'Z'
        
        Return:
        dZ -- gradient of the cost with respect to Z
        """
        Z = activation_cache
        A, _ = NNUtils.sigmoid(Z)
        dZ = dA * A * (1 - A)

        return dZ

    @staticmethod
    def relu(Z):
        """
        Compute the relu of Z
        Argument:
        Z -- A scalar or numpy array of any size
        
        Return:
        A -- post-activation parameter, of the same shape as Z
        Z -- pre-activation parameter
        """
        A = np.maximum(0, Z)
        return A, Z

    @staticmethod
    def relu_backward(dA, activation_cache):
        """
        Implement the backward propagation for a single relu
        
        Arguments:
        dA -- post-activation gradient, of any shape
        activation_cache -- 'Z'
        
        Returns:
        dZ -- gradient of the cost with respect to Z
        """
        Z = activation_cache
        dZ = np.array(dA, copy = True) # just converting dZ to a correct object
        
        # when Z <= 0, you should set dZ to 0 as well
        dZ[Z <= 0] = 0
        return dZ

    @staticmethod
    def linear_forward(A, W, b):
        """
        Arguments:
        A -- activations of previous layer (or input data): (size of previous layer, number of examples)
        W -- weights matrix: numpy array of shape (size of current layer, size of previous layer)
        b -- bias vector: numpy array of shape (size of current layer, 1)
        
        Returns:
        Z -- the input of activation function, also called pre-activation parameter
        cache -- a python tuple containing 'A', 'W', 'b'
        """
        Z = np.dot(W, A) + b
        cache = (A, W, b)
        return Z, cache

    @staticmethod
    def linear_activation_forward(A_previous, W, b, activation):
        """
        Implement the forward propagation for the LINEAR->ACTIVATION layer
        
        Arguments:
        A_previous -- activations from previous layer (or input data): (size of previous layer, number of examples)
        W -- weights matrix: numpy array of shape (size of current layer, size of previous layer)
        b -- bias vector: numpy array of shape (size of current layer, 1)
        activation -- the activation to be used in this layer, stored as a text string 
        
        Returns:
        A -- the output of the activation function, also called the post-activation value
        cache -- containing linear cache, activation cache
        """
        if activation == 'sigmoid':
            Z, linear_cache = NNUtils.linear_forward(A_previous, W, b)
            A, activation_cache = NNUtils.sigmoid(Z)
        elif activation == 'relu':
            Z, linear_cache = NNUtils.linear_forward(A_previous, W, b)
            A, activation_cache = NNUtils.relu(Z)
        return A, (linear_cache, activation_cache)

    @staticmethod
    def l_model_forward(X, parameters):
        """
        Implement forward propagation for the [LINEAR -> RELU] * (L - 1) -> LINEAR -> SIGMOID computation
        Arguments:
        X -- data, numpy array of shape (input size, number of examples)
        parameters -- output of initialize parameter deep
        
        Returns:
        AL -- last post-activation value
        caches -- list of cache
        """
        caches = []
        A = X
        L = len(parameters)//2
        
        for l in range(1, L + 1):
            A_previous = A
            if l < L:
                # implement [LINEAR -> RELU] * (L -1). Add cache to the caches list
                W = parameters.get('W'+str(l))
                b = parameters.get('b'+str(l))
                A, cache = NNUtils.linear_activation_forward(A_previous, W, b, activation = 'relu')
                caches.append(cache)
            elif l == L:
                # implement LINEAR -> SIGMOID. Add cache to the caches list
                W = parameters.get('W'+str(L))
                b = parameters.get('b'+str(L))
                A, cache = NNUtils.linear_activation_forward(A_previous, W, b, activation = 'sigmoid')
                caches.append(cache)
                
        return A, caches

    @staticmethod
    def compute_cost(AL, y):
        """
        Implement the cost function defined by equation
        
        Arguments:
        AL -- probability vector corresponding to your label predictions, shape(1, number of examples)
        y -- true label vector, shape(1, number of examples)
        
        Return:
        cost -- cross entropy cost
        """
        # number of examples
        m = y.shape[1]
        
        logprobs = - (np.multiply(y, np.log(AL)) + np.multiply(1 - y, np.log(1 - AL)))
        cost = np.sum(logprobs)/m
        return cost

    @staticmethod
    def linear_backward(dZ, linear_cache):
        """
        Implement the linear portion of backward propagation for a single layer (layer l)
        
        Arguments:
        dZ -- Gradient of the cost with respect to the linear output (of current layer l)
        linear_cache -- tuple of values (A_pre, W, b) coming from the forward propagation in the current layer
        
        Returns:
        dA_pre -- gradient of the cost with respect to the activation (of the previous layer l - 1), same
        shape as A_pre
        dW -- gradient of the cost with respect to W (current layer l), same shape as W
        db -- gradient of the cost with respect to b (current layer l), same shape as b
        """
        A_pre, W, b = linear_cache
        m = A_pre.shape[1]
        
        dW = np.dot(dZ, A_pre.T)/m
        db = np.sum(dZ, axis=1, keepdims = True)/m
        dA_pre = np.dot(W.T, dZ)
        return dA_pre, dW, db

    @staticmethod
    def linear_activation_backward(dA, cache, activation):
        """
        Implement the backward propagation for the LINEAR -> ACTIVATION layer
        
        Arguments:
        dA -- post-activation gradient for current layer l
        cache -- tuple of (linear cache, activation cache)
        activation -- the activation to be used in this layer, stored as a text string: 'sigmoid' or 'relu'
        
        Returns:
        dA_prev -- gradient of the cost with respect to the activation (of the previous layer l - 1), same
        shape as A_prev
        dW -- gradient of the cost with respect to W (current layer l), same shape as W
        db -- gradient of the cost with respect to b (current layer l), same shape as b
        """
        linear_cache, activation_cache = cache
        
        if activation == 'relu':
            dZ = NNUtils.relu_backward(dA, activation_cache)
            dA_prev, dW, db = NNUtils.linear_backward(dZ, linear_cache)
        elif activation == 'sigmoid':
            dZ = NNUtils.sigmoid_backward(dA, activation_cache)
            dA_prev, dW, db = NNUtils.linear_backward(dZ, linear_cache)

        return dA_prev, dW, db

    @staticmethod
    def l_model_backward(AL, y, caches):
        """
        Implement the backward propagation for the [LINEAR -> RELU] * (l - 1) -> LINEAR -> SIGMOID
        
        Arguments:
        AL -- probability vector, output of the forward propagation (l_model_forward() function)
        y -- true 'label' vector
        caches -- list of caches
        
        Returns:
        grads -- a dictionary with the gradients
                grads['dA' + str(l)] = ...
                grads['dW' + str(l)] = ...
                grads['db' + str(l)] = ...
        """
        grads = {}
        L = len(caches)
        m = y.shape[1]
        
        dAL = -(np.divide(y, AL) - np.divide(1 - y, 1 - AL))
        current_cache = caches[L - 1]
        
        temp_dA_prev, temp_dW, temp_db = NNUtils.linear_activation_backward(
                                        dAL, 
                                        current_cache, 
                                        activation = 'sigmoid')

        grads['dA' + str(L - 1)] = temp_dA_prev
        grads['dW' + str(L)] = temp_dW
        grads['db' + str(L)] = temp_db

        for l in reversed(range(L - 1)):
            current_cache = caches[l]
            temp_dA_prev, temp_dW, temp_db = NNUtils.linear_activation_backward(
                                            grads['dA'+str(l + 1)], 
                                            current_cache, 
                                            activation = 'relu')

            grads['dA' + str(l)] = temp_dA_prev
            grads['dW' + str(l + 1)] = temp_dW
            grads['db' + str(l + 1)] = temp_db
        
        return grads

    @staticmethod
    def update_parameters(parameters, grads, learning_rate):
        """
        Update parameters using gradient descent
        Arguments:
        parameters -- your parameters
        grads -- output of l_model_backward
        learning_rate -- learning rate
        
        Return:
        parameters -- update your parameters
        """
        L = len(parameters)//2
        for l in range(L):
            parameters['W' + str(l + 1)] -= learning_rate * grads['dW' + str(l + 1)] 
            parameters['b' + str(l + 1)] -= learning_rate * grads['db' + str(l + 1)]

        return parameters

    @staticmethod
    def l_layer_model(
        X, 
        y, 
        layer_dims, 
        learning_rate = 0.0075, 
        num_iterations = 3000, 
        print_cost = False
        ):
        """
        Implement a L-layer neural network: [LINEAR -> RELU] * (L - 1) -> LINEAR -> SIGMOID
        
        Arguments:
        X -- data, numpy array of shape (input size, number of examples)
        y -- true 'label' vector of shape (1, number of examples)
        layer_dims -- list containing the input size and each layer size
        learning_rate -- learning rate of the gradient descent update rule
        num_iterations -- number of iterations of the optimization loop
        print_cost -- if true, it prints the cost every 100 steps
        
        Return:
        parameters -- parameters learned by the model. They can then be used to predict
        """
        np.random.seed(1)
        
        # parameters initialize 
        parameters = NNUtils.initialize_parameters_deep(layer_dims)
        
        # keep track of cost
        costs = []
        
        # loop
        for i in range(num_iterations):
            # forward propagation [LINEAR -> RELU] * (L - 1)
            AL, caches = NNUtils.l_model_forward(X, parameters)
            
            # compute cost
            cost = NNUtils.compute_cost(AL, y)
            if print_cost and i % 100 == 0:
                print(f'==cost={cost} after {i} iterations')
                costs.append(cost)
            
            # backward propagation
            grads = NNUtils.l_model_backward(AL, y, caches)
            
            # update parameters
            parameters = NNUtils.update_parameters(parameters, grads, learning_rate)
            
        # plot the cost
        plt.plot(np.squeeze(costs))
        plt.ylabel('cost')
        plt.xlabel('iterations (per hundreds)')
        plt.title("Learning rate =" + str(learning_rate))
        plt.show()
        
        return parameters

    @staticmethod
    def predict(X, parameters):
        """
        Arguments:
        parameters -- containing learned parameters
        X -- input data of size (n_x, m)
        
        returns:
        predictions -- vector of predictions of our model
        """
        AL, cache = NNUtils.l_model_forward(X, parameters)
        predictions = np.where(AL >= 0.5, 1, 0)
        
        return predictions

    @staticmethod
    def measure_accuracy(predictions, y):
        return float(np.sum(predictions == y)/y.shape[1]) * 100

    @staticmethod
    def predict_image_path(file_path, parameters):
        try:
            num_px = 64
            image = np.array(imageio.imread(file_path))
            plt.imshow(image)
            image_reshape = np.resize(image, (3 * num_px**2, 1))/255
            prediction = NNUtils.predict(image_reshape, parameters)
            return prediction
        except Exception as e:
            print(e)
            return None